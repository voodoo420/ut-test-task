import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ut_test/data/repositories/currency_repository_dio_impl.dart';
import 'package:ut_test/domain/repositories/currency_repository.dart';
import 'package:ut_test/domain/usecases/get_bitcoin_exchange_rate.dart';
import 'package:ut_test/presentation/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _providers = [
    Provider.value(
      value: Dio()
        ..options = BaseOptions(
          connectTimeout: 5000,
          receiveTimeout: 3000,
          sendTimeout: 3000,
          baseUrl: 'https://api.binance.com/api/v3/ticker/24hr',
          responseType: ResponseType.plain,
        ),
    ),
    ProxyProvider<Dio, CurrencyRepository>(
      update: (context, dio, repository) => CurrencyRepositoryDioImpl(dio),
    ),
    ProxyProvider2<Dio, CurrencyRepository, GetBitcoinExchangeRate>(
      update: (context, dio, repository, useCase) =>
          GetBitcoinExchangeRate(repository),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: _providers,
      child: MaterialApp(
        title: 'UT test task',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(),
      ),
    );
  }
}
