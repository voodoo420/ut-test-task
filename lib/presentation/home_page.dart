import 'package:flutter/material.dart';
import 'package:ut_test/presentation/currency_item.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 100,
          decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: 500,
            itemBuilder: (context, index) {
              return CurrencyItem();
            },
          ),
        ),
      ),
    );
  }
}
