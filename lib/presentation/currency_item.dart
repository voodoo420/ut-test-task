import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ut_test/domain/entities/currency.dart';
import 'package:ut_test/domain/usecases/get_bitcoin_exchange_rate.dart';

class CurrencyItem extends StatefulWidget {
  @override
  _CurrencyItemState createState() => _CurrencyItemState();
}

class _CurrencyItemState extends State<CurrencyItem> {
  bool _showRate = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: ElevatedButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(const EdgeInsets.all(10)),
          backgroundColor: MaterialStateProperty.all(
            Colors.blueGrey,
          ),
        ),
        onPressed: () {
          setState(() {
            _showRate = !_showRate;
          });
        },
        child: _showRate
            ? FutureBuilder<Currency>(
                future: Provider.of<GetBitcoinExchangeRate>(context)(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CurrencyItemText('loading...');
                  }
                  if (snapshot.hasError)
                    return Center(
                        child: CurrencyItemText('Error: ${snapshot.error}'));
                  if (!snapshot.hasData) {
                    return Center(child: CurrencyItemText('No data'));
                  }
                  return CurrencyItemText(
                      snapshot.data!.currentPrice.toString());
                },
              )
            : CurrencyItemText('Press Me'),
      ),
    );
  }
}

class CurrencyItemText extends StatelessWidget {
  final String text;

  const CurrencyItemText(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(color: Colors.white),
      textAlign: TextAlign.center,
      maxLines: 1,
    );
  }
}
