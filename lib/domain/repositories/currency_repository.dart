import 'package:ut_test/domain/entities/currency.dart';

abstract class CurrencyRepository{
  Future<Currency> getCurrentBitcoinExchangeRate();
}