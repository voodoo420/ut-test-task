import 'package:ut_test/domain/entities/currency.dart';
import 'package:ut_test/domain/repositories/currency_repository.dart';

class GetBitcoinExchangeRate {
  final CurrencyRepository currencyRepository;

  GetBitcoinExchangeRate(this.currencyRepository);

  Future<Currency> call() => currencyRepository.getCurrentBitcoinExchangeRate();
}
