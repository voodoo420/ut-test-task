class Currency{
  final String name;
  final double currentPrice;

  Currency(this.currentPrice, this.name);
}