import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:ut_test/domain/entities/currency.dart';
import 'package:ut_test/domain/repositories/currency_repository.dart';

class CurrencyRepositoryDioImpl extends CurrencyRepository {
  final Dio dio;

  CurrencyRepositoryDioImpl(this.dio);

  @override
  Future<Currency> getCurrentBitcoinExchangeRate() async {
    try {
      final Response response = await dio.get("");
      final List<dynamic> parsed = json.decode(response.toString());
      final price = parsed
          .firstWhere((element) => element['symbol'] == 'BTCUSDT')['lastPrice'];
      return Future.value(Currency(double.parse(price), "bitcoin"));
    } catch (e) {
      print(e);
      throw Exception();
    }
  }
}
